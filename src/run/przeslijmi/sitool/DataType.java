package run.przeslijmi.sitool;

/**
 * Utilities to work with data types.
 *
 * <h2>Check if variable is scalar or is array of scalars</h2>
 * <pre><code>
 *     boolean isScalar = DataType.isScalar("value");
 *     boolean isArrayOfScalars = DataType.isArrayOfScalars(new String[]{"aaa", "bbb"});
 * </code></pre>
 *
 * @version 1.0
 */
public class DataType {

    /**
     * Checks if given value is a scalar.
     *
     * @param object Any value to be checked.
     * @return boolean
     * @since 1.0
     */
    public static boolean isScalar(Object object) {

        if (object instanceof Byte) return true;
        if (object instanceof Character) return true;
        if (object instanceof Short) return true;
        if (object instanceof Integer) return true;
        if (object instanceof Long) return true;
        if (object instanceof Float) return true;
        if (object instanceof Double) return true;
        if (object instanceof Boolean) return true;
        if (object instanceof String) return true;

        return false;
    }

    /**
     * Checks if given value is a an array of scalar value.
     *
     * @param object Any value to be checked.
     * @return boolean
     * @since 1.0
     */
    public static boolean isArrayOfScalars(Object object) {

        if (object instanceof byte[]) return true;
        if (object instanceof char[]) return true;
        if (object instanceof short[]) return true;
        if (object instanceof int[]) return true;
        if (object instanceof long[]) return true;
        if (object instanceof float[]) return true;
        if (object instanceof double[]) return true;
        if (object instanceof boolean[]) return true;
        if (object instanceof String[]) return true;

        return false;
    }
}

package run.przeslijmi.sitool;

import org.junit.jupiter.api.Test;
import java.util.HashSet;
import static org.junit.jupiter.api.Assertions.*;

class DataTypeTest {

    @Test
    void isScalar() {

        // positive
        assertTrue(DataType.isScalar((byte) 100), "but byte is a scalar");
        assertTrue(DataType.isScalar('t'), "but char is a scalar");
        assertTrue(DataType.isScalar((short)2), "but short is a scalar");
        assertTrue(DataType.isScalar(5), "but int is a scalar");
        assertTrue(DataType.isScalar((long)500), "but long is a scalar");
        assertTrue(DataType.isScalar((float)5.12), "but float is a scalar");
        assertTrue(DataType.isScalar(5.12), "but double is a scalar");
        assertTrue(DataType.isScalar(true), "but bool is a scalar");
        assertTrue(DataType.isScalar("Test"), "but String is a scalar");

        // negative
        assertFalse(DataType.isScalar(new Object()), "but Object is not a scalar");
        assertFalse(DataType.isScalar(new HashSet<String>()), "but Object is not a scalar");
    }

    @Test
    void isArrayOfScalarsPositive() {

        byte[] bytes = new byte[]{10, 20, 30};
        assertTrue(DataType.isArrayOfScalars(bytes), "but bytes is a array of scalar");

        char[] chars = new char[]{'a', 'b', 'c'};
        assertTrue(DataType.isArrayOfScalars(chars), "but chars is a array of scalar");

        short[] shorts = new short[]{(short)2, (short)4, (short)6};
        assertTrue(DataType.isArrayOfScalars(shorts), "but shorts is a array of scalar");

        int[] ints = new int[]{2, 4, 6};
        assertTrue(DataType.isArrayOfScalars(ints), "but ints is a array of scalar");

        long[] longs = new long[]{(long)2, (long)4, (long)6};
        assertTrue(DataType.isArrayOfScalars(longs), "but longs is a array of scalar");

        float[] floats = new float[]{(float)2, (float)4, (float)6};
        assertTrue(DataType.isArrayOfScalars(floats), "but floats is a array of scalar");

        double[] doubles = new double[]{2.12, 4.12, 6.12};
        assertTrue(DataType.isArrayOfScalars(doubles), "but doubles is a array of scalar");

        boolean[] booleans = new boolean[]{true, true, false};
        assertTrue(DataType.isArrayOfScalars(booleans), "but booleans is a array of scalar");

        String[] strings = new String[]{"test1", "test2", "test3"};
        assertTrue(DataType.isArrayOfScalars(strings), "but Strings is a array of scalar");
    }

    @Test
    void isArrayOfScalarsNegative() {

        Object[] Objects = new Object[]{new Object(), new Object(), new Object()};
        assertFalse(DataType.isArrayOfScalars(Objects), "but Objects is a array of scalar");

        assertFalse(DataType.isScalar(new HashSet<String>()), "but HashSet of Strings is not a scalar");
    }
}